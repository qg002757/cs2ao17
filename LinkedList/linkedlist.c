/*#include <stdio.h>
#include <stdlib.h>
typedef struct linkedlist{
    int data;
    struct linkedlist *Next;
} LinkedList;

void insert(LinkedList *temp, int data){
    LinkedList *tempNode = malloc(sizeof(LinkedList));
    tempNode->data = data;
    tempNode->Next = &(*temp);
    temp = tempNode;
}

void traverLinkedList(LinkedList *temp){
    while(temp != NULL){
        printf("%d\n",temp->data);
        temp = temp->Next;
    }
    printf("Is null");
}

int main(){
    LinkedList *head = NULL;
    int forLinkedList[5] = {1,2,3,4,5};
    insert(head,forLinkedList[0]);
    traverLinkedList(head);
}
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct linkedlist {
    int data;
    struct linkedlist* Next;
} LinkedList;

void insert(LinkedList* head, int data) {
    LinkedList* tempNode = malloc(sizeof(LinkedList));
    tempNode->data = data;
    tempNode->Next = head;
    head = tempNode;
}

void traverseLinkedList(LinkedList* temp) {
    while (temp != NULL) {
        printf("%d\n", temp->data);
        temp = temp->Next;
    }
    printf("Is null\n");
}

int main() {
    LinkedList* head = NULL;
    int forLinkedList[5] = {1, 2, 3, 4, 5};

    for (int i = 0; i < 5; i++) {
        insert(head, forLinkedList[i]);
    }

    traverseLinkedList(head);

    return 0;
}