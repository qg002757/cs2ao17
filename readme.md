# CS2AO17

# Factorial using iterative and rtecursive
```javascript:
function factorial(n){
    temp = n
    for (let i = n; i >= 2; i--){
        temp *=(i-1)
    }
    console.log(temp)
}

factorial(4)

function factorialR(n){
    if(n == 1){
        return n
    }
    return n * factorialR(n-1)
}

console.log(factorialR(4))
```

# In class excercise
<img src='/Images/image.png' alt='Image'>

### Pseudocode
```
function = n
check if n is 1, and if it is return n
otherwise return current n + function(n-1)

```

```javascript:
//sum of numbers to n with recursion

function recursionSum(n){
    if(n == 1){
        return n
    }
    return n + recursionSum(n-1)
}

console.log(recursionSum(5))
```

# Algorithms/data structre we have seen and know about include:
1. Binary tree
2. In-order
3. pre-order
4. Post-order
5. Heap
6. Divide and conquer
7. Max-Min