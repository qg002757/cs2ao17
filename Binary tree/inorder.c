/*
Binary tree is a set of finte nodes. It can contain 0 or many nodes.
We have the root node at the top. There are seevral ways to traverse a
node and these are:
1.) In-order - Traverse the left, then root then right
2.) Pre-order
3.) Post-order

These are all depth-first type search
*/

#include <stdio.h>
#include <stdlib.h>

struct Node{
    int data;
    struct Node *left;
    struct Node *right;
};

struct Node *createNode(int data){
    //create space in memeory for node
    struct Node *firstNode = malloc(sizeof(struct Node));

    firstNode->data = data;
    firstNode->left = NULL;
    firstNode->right = NULL;
    return firstNode;
}

void inorder(struct Node *temp){
    if(temp == NULL) return;
    inorder(temp->left);
    printf("%d\n",temp->data);
    //check right node
    inorder(temp->right);
}

void preorder(struct Node *temp){
    if(temp == NULL) return;
    printf("%d\n",temp->data);
    preorder(temp->left);
    preorder(temp->right);
}

void postorder(struct Node *temp){
    //this requires traversal of depest node first
    if(temp == NULL) return;

    postorder(temp->left);
    postorder(temp->right);

    printf("%d",temp->data);
}

int main(){
    struct Node *root = createNode(1);
    root->left = createNode(2);
    root->right = createNode(3);
    root->left->left = createNode(4);
    root->left->right = createNode(5);
    root->right->left = createNode(6);
    root->right->right = createNode(7);

    /*
    1
   / \ 
  2   3
/ \  / \
4  5 6  7

inorder will be:
4251636

preorder will be:
1245367
       */
    printf("\nInorder\n");
    inorder(root);
    printf("\npreorder\n");
    preorder(root);

    
}