/*
Heap a type of binary tree data structure, and they are two types:
1. Max heap
2. Min heap

Lets implements heap
*/

#include <stdio.h>
#include <stdlib.h>
struct heap{
    int data;
    struct heap *left;
    struct heap *right;
};

int heap(struct heap *myHeap,int values[], int point){
    //length of our array
    //https://www.geeksforgeeks.org/length-of-array-in-c/
    
}

void traversHeap(struct heap *myHeap){
    if(myHeap->left == NULL){
        printf("%d",myHeap->data);
        return;
    }
    traversHeap(myHeap->left);
    printf("%d",myHeap->data);
    traversHeap(myHeap->right);
}

void insertElement(){
    //As we know, for heap we have max heap and min heap, to insert element, insert it at the bottom of the binary tree
}


int main(){
    int forHeap[] = {8,4,6,3,2,3};
    struct heap *root = malloc(sizeof(struct heap));
    struct heap *firstLeftChild = malloc(sizeof(struct heap));
    struct heap *firstRightChild = malloc(sizeof(struct heap));
    struct heap *secondLeftChild = malloc(sizeof(struct heap));
    struct heap *secondRightChild = malloc(sizeof(struct heap));
    struct heap *thirdLeftChild = malloc(sizeof(struct heap));
    root->data = 8;
    root->left = firstLeftChild;
    root->right = firstRightChild;

    firstLeftChild->data = 4;
    firstLeftChild->left = secondLeftChild;
    firstLeftChild->right = secondRightChild;

    firstRightChild->data = 6;
    firstRightChild->left = thirdLeftChild;
    firstRightChild->right = NULL;

    secondLeftChild->data = 3;
    secondLeftChild->left = NULL;
    secondLeftChild->right = NULL;

    secondRightChild->data=2;
    secondRightChild->left = NULL;
    secondRightChild->right = NULL;

    traversHeap(root);

    return 0;
}

