/*
Given a number n, print all numbers smaller than or equals to n (assuming we use abs(), so no negatives)
*/
#include <stdio.h>
int SEO(int val){
    if(val == 0){
        printf("\n%d\n",0);
        return 0;
    }
    printf("\n%d\n", val);
    return SEO(val-1);
}

int main(){
    SEO(5);
}