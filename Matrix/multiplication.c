#include <stdio.h>

/*
Given a 2x2 matrices, multiple them
*/

//Will implement another method later

void matMultiply(){
    int mat1[2][2] = {
        {1,2},
        {3,4}
    };
    int mat2[2][2] = {
        {5,6},
        {7,8}
    };
    int res[2][2];

    //change res matrix all to 0s

    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++){
            res[i][j] = 0;
        }
    }

    for(int i=0; i<2; i++){
        //out for loop
        for(int j=0; j<2; j++){
            for(int k=0; k<2; k++){
                res[i][j] += (mat1[i][k]*mat2[k][j]);
                printf("%i\t%i\n",mat1[i][k],mat2[k][j]);
            }
        }

    }

    //print out our result
    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++){
            printf("%d\t",res[i][j]);
        }
        printf("\n");
    }
}

int main(){
    matMultiply();
}