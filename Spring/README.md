# Spring Coursework
Submit a report (limit is 10 pages) of type PDF

# Convert current into .tex

# Questions
1. Task 1:  Provide examples illustrating how the chmod command can be employed
 to modify file permissions, considering scenarios involving the owner, group, 
and others. Using symbolic notation, how can the chmod command be applied to 
grant the owner of a directory named "private_data" read and write permissions,
 allow the group to have read-only access, and deny any permissions to others?
 The changes should be applied recursively to all files and subdirectories within
 "private_data.
**Solution:** Environment used for running the code: Operating system -> Linux, 
Intepreter -> Bash 
## How does chmod work - 
Well, given a file/directory listed, we have the permission
for user, group and others is shown, e.g.
<img src='./images/Screenshot 2024-02-13 185823.png' alt='1'/>

Key
<img src='./images/Screenshot 2024-02-21 125308.png' alt='3'/>
Explain diagram
The chmod system call according to the manual chnages the file mode bit of each
given file according to mode. According to the GNU page, file mode bits have two
parts: the file permission bit which control ordinary access to the file and
special mode bits, whihc affect only some files. So file mode bits are a bit
that contains binary flags. Access to files is managed through the file permissions, attributes and ownerships ensuring only authorised users and processes can access files and directories.
[https://www.gnu.org/software/coreutils/manual/html_node/Mode-Structure.html] 

<img src='./images/Screenshot 2024-02-13 193254.png' alt='2'/>
Explain diagram

Add Key to explain What the stuff do like u,g,o,r,w,x
Above is for file permission

Below is for directory part
Similary to files, directory can be managed using chmod. Directory utilises the same symbolic notation, but one important thing to note is in order to recursively apply changes to files and subdirectories, the 'R' option should be used
Also give an example

2. Task 2: Explain with code how strace command can be used to count the 
number of system calls of the ls command. Add the – o switch to the strace 
call in exercise E5 (refer to E5 in lab manual week 1) to direct strace 
output to a file and include a summary of the output together with a brief 
description of what you observe in your report.

**Solution:** Environment used for running the code: Operating system -> Linux (WSL), 
Intepreter -> Bash 
```bash
strace -c -o strace_output.txt ls
cat strace_output.txt
```
**Eplanatino**
In the above code, we use the strace command whihc is  powerful tool used to trace system calls and signals. The '-c' option tells strace to produce a summary count of each system calls and report it at the end of the trace. The '-o strace_output.txt' option directs the output of the strace command to a file named 'strace_output.txt'. The 'ls' command is the command we want to trace. We use the cat to view the output of the file

3. Task 3: Write a shell script to simulate a simple temperature monitoring system. Prompt the user to input the current temperature in Celsius. The script should then print a message indicating the weather condition based on the entered temperature. If the temperature is 25 degrees Celsius or higher, the script will print "Warm weather"; otherwise, print "Cool weather."

```bash
#!/bin/bash
read "Enter Temperature " userInput

if [[ "$userInput" -ge 25 ]]; then
    echo "Warm weather"
else
    echo "Cool weather"
fi
```

4. Task 4: Detail the series of system calls that a command interpreter or shell must execute to initiate a new process in an operating system. Explain the purpose and significance of each relevant system call in the process creation mechanism. Support your explanation with a concise yet comprehensive example that illustrates the step-by-step execution of these system calls during the creation of a new process.
In order for a shell to initiate a new process, it typically executes a series of system calls, being:
1. fork() - The purpose of this system call is to create a new process by duplicating the current process known as the parent process. The new process is known as the child proces. Significance of this system call is to allow for the creation of a new process that inherits resources such as memory, file descriptors, and othe atttributes if the parent process.
```c
    #include <stdio.h>
    #include <unistd.h>

    int main() {
        pid_t pid = fork(); // Create a new process

        if (pid == 0) {
            printf("Child process\n");
        } else if (pid > 0) {
            printf("Parent process\n");
        } else {
            printf("Error occurred\n");
        }

        return 0;
}

```
2. exec() - The purpose of this system call is to replace the current process image with a new process imahge. It loads a new program into the current process address space and begins eecutions. The significance of this system call iit enables the child process to eecute a different program from the parent process. It is comminly used to ececute commands enetered by users in the shell

```c
    #include <stdio.h>
    #include <unistd.h>

    int main() {
        pid_t pid = fork();

        if (pid == 0) {
            printf("Child process\n");
            execl("/bin/ls", "ls", "-l", NULL); // Execute the ls command
        } else if (pid > 0) {
            printf("Parent process\n");
        } else {
            printf("Error occurred\n");
        }

        return 0;
}
```

3. wait()/waitpid - The purpose of this system call is after the child process terminates, the parent process usually waits for the child process to finish before proceeding with its execujtion. The wait()/waitpid() system calls allow the parent process to wait for the child process to exit and collects its termination status. The signifiance of this system call is to ensure synchronization between parens and child processes, alloing the parent to handle termination properly

```c
    #include <stdio.h>
    #include <unistd.h>
    #include <sys/wait.h>

int main() {
    pid_t pid = fork();

    if (pid == 0) {
        printf("Child process\n");
        execl("/bin/ls", "ls", "-l", NULL);
    } else if (pid > 0) {
        printf("Parent process\n");
        wait(NULL); // Wait for the child process to complete
        printf("Child process terminated\n");
    } else {
        printf("Error occurred\n");
    }

    return 0;
}
```

5. Task 5: Explain the distinctions between the getpid() and getppid() functions in the context of process identification in C programming. Provide a detailed example to illustrate the usage of both functions, showcasing how they retrieve the process ID and parent process ID, respectively. Your response should highlight the significance of these functions in process management.

# Reference
[https://linuxize.com/post/chmod-command-in-linux/]
[https://www.gnu.org/software/coreutils/manual/html_node/Mode-Structure.html]
[https://courses.cs.duke.edu/spring05/cps210/Lab1.html]