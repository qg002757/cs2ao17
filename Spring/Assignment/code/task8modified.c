#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
    int pfds1[2], pfds2[2];
    pipe(pfds1);
    pipe(pfds2);

    if (!fork()) {
        // Child process for ls
        close(1);
        dup(pfds1[1]);
        close(pfds1[0]);
        close(pfds1[1]);
        execlp("ls", "ls", NULL);
    } else {
        if (!fork()) {
            // Child process for cut
            close(0);
            dup(pfds1[0]);
            close(1);
            dup(pfds2[1]);
            close(pfds1[0]);
            close(pfds1[1]);
            close(pfds2[0]);
            close(pfds2[1]);
            execlp("cut", "cut", "-b", "1-3", NULL);
        } else {
            // Parent process
            close(0);
            dup(pfds2[0]);
            close(pfds1[0]);
            close(pfds1[1]);
            close(pfds2[0]);
            close(pfds2[1]);
            execlp("sort", "sort", NULL);
        }
    }

    return 0;
}
