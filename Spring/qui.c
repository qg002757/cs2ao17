#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int counter = 0;

int main() {
	pid_t pid;//, pid2;

    // Fork a child process
	pid = fork();
	//pid2 = fork();
	if (pid < 0) {
        // Fork failed
		fprintf(stderr, "Fork failed\n");
		return 1;
    	}
    	/*if (pid == 0) {
        // Child process
        	for (int i = 0; i < 10; i++) {
            		printf("child %d process: counter=%d\n", getpid(), ++counter);
            		sleep(1);
		}*/
        if (pid == 0) {
        // Child process
		for (int i = 0; i < 10; i++) {
                	printf("child %d process: counter=%d\n", getpid(), ++counter);
                	sleep(1);
		}
        }
	else{
		for(int i = 0; i < 10; i++) {
			printf("parent %d process: counter=%d\n", getpid(), ++counter);
            		sleep(1);
        }
        // Wait for the child to finish
        wait(NULL);
	}

    return 0;
}

