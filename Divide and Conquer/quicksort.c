/*
This algorithm is simple, just partition and it uses recursion
*/
#include <stdio.h>


void quickSort(int nums[], int low, int high);
int partition(int nums[], int low, int high);
void swap(int * num1, int *num2);

void swap(int * num1, int *num2){
    int temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}

int partition(int nums[], int low, int high){
    int anchor = low -1;
    int pivot = nums[high];
    for(int i=low; i<high; i++){
        if(nums[i] <= pivot){ //remember pivot is high
            //printf("Before swap num1:%d, num2:%d", nums[i],nums[anchor]);
            anchor++;
            swap(&nums[i],&nums[anchor]);
            //printf("After swap num1:%d, num2:%d\n", nums[i],nums[anchor]);
        }
    }
    swap(&nums[anchor + 1],&nums[high]);
    return anchor +1;
}

void quickSort(int nums[], int low, int high){
    int temp;
    if(low < high){
        temp = partition(nums, low, high);
        quickSort(nums,low,temp-1);
        quickSort(nums,temp+1,high);
    }
}
//main
int main(){
    int test[] = {10,80,30,90,40,200,500,600};
    int len = *(&test + 1) - test;
    quickSort(test,0,len-1);
    for(int i=0; i<len; i++){
        printf("%d ",test[i]);
    }
}