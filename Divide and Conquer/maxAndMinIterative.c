#include <stdio.h>

int * maxAndMin(int arr[], int length){
    int min = arr[0];
    int max = arr[0];
    static int res[2];

    for(int i=1; i<length; i++){
        if(arr[i] > max) max = arr[i];//for max
        if(arr[i] < min) min = arr[i];//for min
    }

    res[0] = min;
    res[1] = max;

    return res;
}

int main(){
    int test[] = {1,2,3,4,5,6};
    int length = *(&test+1)-test;
    //printf("%d",length);
    int *pointer = maxAndMin(test,length);
    for(int i=0; i<2; i++){
        printf("%d\n",*(pointer+i));
    }
}