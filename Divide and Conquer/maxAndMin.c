/*
This algorithm is one that allows us to find the maximum and minimum within a given array
So the algorithm is:
FOR MAXIMUM
1.) First we check whether the array is of lenght 1, if true, return array[0] as max
2.) If array is not of length 0, then we check whether the index is greater than or equals to [length of array - 2], if true, compare array[index] with array[index+1] and return whihc ever
is largest
3.) If index is not geater than or equals to lenth -2, then use recursion, pass int he array, the index+1 and length of the array
4.) If recursion succeeds, we compare the value at array[index] with max whihc was returned by recursion


reaso why it is length-2, is beacuse we want to first compare the lat 2 elements and then go left using recursion, and then store the largest in a max varibale
*/
#include <stdio.h>

int Max(int array[], int index, int length){
    int max;
    //check if array is of length 1
    if(length == 1) return array[index];
    //check if array is length -2 to firsty compare last two element of our array
    if(index >= length -2){
        if(array[index] > array[index+1]) return array[index];
        return array[index+1];
    } 
    // if above are not true, use recursion
    max = Max(array, index+1, length);
    if(array[index] > max) return array[index];
    return max;

}

int Min(int array[], int index, int length){
    int min;
    //check if array is of length 1
    if(length == 1) return array[index];
    //check if array is length -2 to firsty compare last two element of our array
    if(index>= length-2){
        if(array[index] < array[index+1]) return array[index];
        return array[index+1];
    }
    min = Min(array, index+1, length);
    if(array[index]< min) return array[index];
    return min;
}

int main(){
    int nums[] = {1,200,500,4,500,60,800};
    int len = *(&nums +1)-nums;
    printf("Maximum is %d\n",Max(nums,0,len));
    printf("Minimum is %d\n", Min(nums,0,len));
}